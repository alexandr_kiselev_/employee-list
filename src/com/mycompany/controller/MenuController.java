package com.mycompany.controller;

import com.mycompany.beans.EmployeeType;

import java.util.Scanner;

/**
 * Class for work with main menu.
 */
public class MenuController {

  private Scanner scan;
  private EmployeeController employeeController;

  public MenuController() {
    employeeController = new EmployeeController();
    scan = new Scanner(System.in);
  }

  /**
   * Printing main menu.
   */
  public void printMenu() {
    String current;
    do {
      System.out.println("Menu :");
      System.out.println("1. View list of employee");
      System.out.println("2. Add employee");
      System.out.println("3. Remove employee");
      System.out.println("4. Change type of employee");
      System.out.println("5. Set manager for employee");
      System.out.println("6. Sort list by last name");
      System.out.println("7. Sort list by date of employment");
      System.out.println("8. Load list from file");
      System.out.println("9. Save list to file");
      System.out.println("0. Exit");
      System.out.print("Select item, please: ");
      current = scan.nextLine();

      switch (current) {
        case "1":
          employeeController.printEmployeeList();
          break;
        case "2":
          printSubmenuForAddEmployee();
          break;
        case "3":
          employeeController.removeEmployee();
          break;
        case "4":
          employeeController.changeTypeEmployee();
          break;
        case "5":
          employeeController.setManagerForEmployee();
          break;
        case "6":
          employeeController.sortEmployeeListByLastName();
          break;
        case "7":
          employeeController.sortEmployeeListByDateEmployment();
          break;
        case "8":
          employeeController.loadEmployeeListFromFile();
          break;
        case "9":
          employeeController.saveEmployeeListToFile();
          break;
        default:
          break;
      }

    } while (!current.equals("0"));
  }

  /**
   * Printing submenu.
   */
  private void printSubmenuForAddEmployee() {
    String choice;
    do {
      System.out.println("1. Worker");
      System.out.println("2. Manger");
      System.out.println("3. Specialist");
      System.out.println("0. Return back");
      System.out.print("Select employee type: ");

      choice = scan.nextLine();

      switch (choice) {
        case "1":
          employeeController.addEmployeeByType(EmployeeType.WORKER);
          break;
        case "2":
          employeeController.addEmployeeByType(EmployeeType.MANAGER);
          break;
        case "3":
          employeeController.addEmployeeByType(EmployeeType.SPECIALIST);
          break;
        default:
          break;
      }

    } while (!choice.equals("0") && !choice.equals("1") && !choice.equals("2") && !choice.equals("3"));
  }
}
