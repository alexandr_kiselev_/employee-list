package com.mycompany.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.mycompany.beans.Employee;
import com.mycompany.beans.Specialist;
import com.mycompany.beans.Worker;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * Class for work with XML file.
 */
public class XMLFileHandler {
  private Document document;
  private String fileName;

  /**
   * Constructor get fileName and parse XML.
   *
   * @param fileName path to file.
   */
  public XMLFileHandler(final String fileName) {
    this.fileName = fileName;
    File xmlFile = new File(fileName);
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder;
    try {
      dBuilder = dbFactory.newDocumentBuilder();
      document = dBuilder.parse(xmlFile);
      document.getDocumentElement().normalize();

    } catch (SAXException | ParserConfigurationException | IOException e1) {
      e1.printStackTrace();
    }
  }

  /**
   * Save all employees from list to XML.
   *
   * @param employeeList list of employees.
   */
  public void saveAllEmployees(final List<Employee> employeeList) {
    DocumentBuilderFactory factory;
    factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(false);

    try {
      document = factory.newDocumentBuilder().newDocument();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
    Element root = document.createElement("Company");

    document.appendChild(root);
    for (Employee employee : employeeList
      ) {
      addElementEmployee(root, employee);
    }

    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = null;
    try {
      transformer = transformerFactory.newTransformer();
    } catch (TransformerConfigurationException e) {
      e.printStackTrace();
    }
    document.getDocumentElement().normalize();
    DOMSource source = new DOMSource(document);
    StreamResult result = new StreamResult(new File(fileName));
    if (transformer != null) {
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      try {
        transformer.transform(source, result);
      } catch (TransformerException e) {
        e.printStackTrace();
      }
    }

    setDocument(document);

  }

  /**
   * Add element with text to XML.
   *
   * @param masterElement - parent element.
   * @param tagName       - name new element.
   * @param text          - text for new element.
   */
  private void addElementWithText(final Element masterElement, final String tagName, final String text) {
    Element element = document.createElement(tagName);
    element.appendChild(document.createTextNode(text));
    masterElement.appendChild(element);

  }

  /**
   * Add employee element to XML.
   *
   * @param root     - root element.
   * @param employee - employee.
   */
  private void addElementEmployee(final Element root, final Employee employee) {
    DateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    Element employeeElement = document.createElement("Employee");
    employeeElement.setAttribute("id", String.valueOf(employee.getId()));

    if (employee instanceof Worker) {
      addElementWithText(employeeElement, "managerId",
        String.valueOf(((Worker) employee).getManagerId()));
    }

    addElementWithText(employeeElement, "lastName", employee.getLastName());
    addElementWithText(employeeElement, "firstName", employee.getFirstName());
    addElementWithText(employeeElement, "patronymic", employee.getPatronymic());
    addElementWithText(employeeElement, "employeeType", employee.getEmployeeType().name());
    addElementWithText(employeeElement, "dateEmployment", format.format(employee.getDateEmployment()));
    addElementWithText(employeeElement, "dateBirth", format.format(employee.getDateBirth()));

    if (employee instanceof Specialist) {
      addElementWithText(employeeElement, "description",
        String.valueOf(((Specialist) employee).getDescription()));
    }

    root.appendChild(employeeElement);
  }

  public Document getDocument() {
    return document;
  }

  private void setDocument(Document document) {
    this.document = document;
  }
}
