package com.mycompany.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Class contain information about manager and list employee.
 */
public class Manager extends Employee {
  /**
   * List of employee.
   */
  private List<Worker> workerList;

  public Manager(final int id, final String lastName, final String firstName, final String patronymic,
                 final Date dateEmployment, final Date dateBirth) {
    super(id, lastName, firstName, patronymic, EmployeeType.MANAGER, dateEmployment, dateBirth);
    this.workerList = new ArrayList<>();
  }

  public List<Worker> getWorkerList() {
    return workerList;
  }

  /**
   * Link worker to manager, add to list of workers.
   *
   * @param worker worker.
   * @return true - success, false - fail.
   */
  public boolean addSubordinateEmployee(final Worker worker) {
    if (this.workerList.add(worker)) {
      worker.setManagerId(this.id);
      return true;
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    if (workerList == null) {
      return "Manager{" +
        "id=" + id +
        ", lastName='" + lastName + '\'' +
        ", firstName='" + firstName + '\'' +
        ", patronymic='" + patronymic + '\'' +
        ", employeeType='" + employeeType.getEmployeeType() + '\'' +
        ", dateEmployment=" + dateEmployment +
        ", dateBirth=" + dateBirth +
        ", workerList=null}";
    } else {
      stringBuilder.append("Manager{" + "id=").append(id).append(", lastName='").append(lastName).append('\'')
        .append(", firstName='").append(firstName).append('\'').append(", patronymic='").append(patronymic).append('\'')
        .append(", employeeType='").append(employeeType.getEmployeeType()).append('\'')
        .append(", dateEmployment=").append(dateEmployment).append(", dateBirth=").append(dateBirth)
        .append(", workerList=\n");
      for (Employee employee :
        workerList)
        stringBuilder.append("\t{").append(employee).append("}\n");
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }

  public String viewSubordinateEmployee() {
    return "Manager{" +
      "id=" + id +
      ", lastName='" + lastName + '\'' +
      ", firstName='" + firstName + '\'' +
      ", patronymic='" + patronymic + '\'' +
      ", employeeType='" + employeeType.getEmployeeType() + '\'' +
      ", dateEmployment=" + dateEmployment +
      ", dateBirth=" + dateBirth +
      ", employeeListSize=" + workerList.size() + "}";
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Manager)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    Manager manager = (Manager) o;
    return Objects.equals(getWorkerList(), manager.getWorkerList());
  }

  @Override
  public int hashCode() {

    return Objects.hash(super.hashCode(), getWorkerList());
  }
}

