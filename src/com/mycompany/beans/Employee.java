package com.mycompany.beans;

import java.util.Date;
import java.util.Objects;

/**
 * Class contain information about employee.
 */
public abstract class Employee {
  /**
   * Id employee.
   */
  protected int id;
  /**
   * Last name.
   */
  protected String lastName;
  /**
   * First name.
   */
  protected String firstName;
  /**
   * Patronymic.
   */
  protected String patronymic;
  /**
   * Type of employee.
   */
  protected EmployeeType employeeType;
  /**
   * Date of employment.
   */
  protected Date dateEmployment;
  /**
   * Date of birth.
   */
  protected Date dateBirth;

  public abstract String toString();

  public int getId() {
    return id;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getPatronymic() {
    return patronymic;
  }

  public EmployeeType getEmployeeType() {
    return employeeType;
  }

  public void setPatronymic(final String patronymic) {
    this.patronymic = patronymic;
  }

  public Date getDateEmployment() {
    return dateEmployment;
  }

  public void setDateEmployment(final Date dateEmployment) {
    this.dateEmployment = dateEmployment;
  }

  public Date getDateBirth() {
    return dateBirth;
  }

  public void setDateBirth(final Date dateBirth) {
    this.dateBirth = dateBirth;
  }

  Employee(final int id, final String lastName, final String firstName, final String patronymic,
           final EmployeeType employeeType, final Date dateEmployment, final Date dateBirth) {
    this.id = id;
    this.lastName = lastName;
    this.firstName = firstName;
    this.patronymic = patronymic;
    this.employeeType = employeeType;
    this.dateEmployment = dateEmployment;
    this.dateBirth = dateBirth;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Employee)) {
      return false;
    }
    Employee employee = (Employee) o;
    return getId() == employee.getId() &&
      Objects.equals(getLastName(), employee.getLastName()) &&
      Objects.equals(getFirstName(), employee.getFirstName()) &&
      Objects.equals(getPatronymic(), employee.getPatronymic()) &&
      getEmployeeType() == employee.getEmployeeType() &&
      Objects.equals(getDateEmployment(), employee.getDateEmployment()) &&
      Objects.equals(getDateBirth(), employee.getDateBirth());
  }

  @Override
  public int hashCode() {

    return Objects.hash(getId(), getLastName(), getFirstName(), getPatronymic(), getEmployeeType(), getDateEmployment(), getDateBirth());
  }
}
