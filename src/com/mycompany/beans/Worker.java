package com.mycompany.beans;

import java.util.Date;

/**
 * Class contain information about Worker.
 */
public class Worker extends Employee {
  /**
   * Id manager.
   */
  private int managerId;

  public Worker(final int id, final int managerId, final String lastName, final String firstName,
                final String patronymic, final Date dateEmployment, final Date dateBirth) {
    super(id, lastName, firstName, patronymic, EmployeeType.WORKER, dateEmployment, dateBirth);
    this.managerId = managerId;
  }

  public int getManagerId() {
    return managerId;
  }

  public void setManagerId(final int managerId) {
    this.managerId = managerId;
  }

  @Override
  public String toString() {
    return "Worker{" +
      "id=" + id +
      ", managerId='" + managerId + '\'' +
      ", lastName='" + lastName + '\'' +
      ", firstName='" + firstName + '\'' +
      ", patronymic='" + patronymic + '\'' +
      ", employeeType='" + employeeType.getEmployeeType() + '\'' +
      ", dateEmployment='" + dateEmployment + '\'' +
      ", dateBirth='" + dateBirth + '\'' +
      '}';

  }
}